package com.example.miprimeraapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

                                                    // 3. Implementar la interface OnClickListener
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // 1. Botón del layout debe ser atributo en la clase
    Button botonLogin, botonRegistrarse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 2. Asociar el atributo con el view del layout usando su id
        botonLogin = findViewById(R.id.BotonIniciarSesion);
        botonRegistrarse = findViewById(R.id.BotonRegistrarse);

        //4. Habilitar el botón para que sea clickeable
        botonLogin.setOnClickListener(this);
        botonRegistrarse.setOnClickListener(this);

    }

    // 5. Implementar el método onClick
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.BotonIniciarSesion) {
            Intent i = new Intent(this,LoginActivity.class);
            startActivity(i);
        }

        else if (view.getId() == R.id.BotonRegistrarse) {
            Intent i = new Intent(this,SignupActivity.class);
            startActivity(i);
        }
    }
}