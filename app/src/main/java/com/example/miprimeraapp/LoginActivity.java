package com.example.miprimeraapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editUsuario, editContraseña;
    private Button botonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editUsuario = (EditText)findViewById(R.id.editUsuario);
        editContraseña = (EditText)findViewById(R.id.editContraseña);
        botonLogin = (Button)findViewById(R.id.botonLogin);

        botonLogin.setOnClickListener(this);
    }

    private String usuario = "Jean";
    private String contraseña = "1234";


    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.botonLogin) {

            if (usuario.equals(editUsuario.getText().toString())) {

                Intent i = new Intent(this,HomeActivity.class);
                startActivity(i);

            }

        }
    }
}